import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {Game} from './Layout.js';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<Game />, document.getElementById('main'));

serviceWorker.unregister();
